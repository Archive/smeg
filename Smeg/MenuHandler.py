#  Smeg - Simple Menu Editor for GNOME
#
#  Travis Watkins <alleykat@gmail.com>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
#  Boston, MA 02111-1307, USA.
#
#  (C) Copyright 2005 Travis Watkins

class MenuHandler(object):
	filename = None
	options = None
	
	def __init__(self, filename, options):
		self.filename = filename
		self.options = options

	def __iter__(self):
		pass

	def getEntries(self, menu):
		pass

	def finish(self):
		pass

	def move(self, item, old_parent, new_parent, before, after):
		pass

	def revert(self, item):
		pass

	def new(self, item, parent):
		pass

	def delete(self, item):
		pass

	def toggleVisibility(self, item):
		pass

	def isVisible(self, item):
		pass

	def save(self, item):
		pass

class MenuItem(object):
	def __init__(self, type, id, name, comment=None, command=None, icon=None, use_term=None, show=None, permissions=None):
		self.type = type
		self.id = id
		self.name = name
		self.comment = comment
		self.command = command
		self.icon = icon
		self.use_term = use_term
		self.show = show
		self.permissions = permissions

	def getType(self):
		return self.type

	def getId(self):
		return self.id

	def getName(self):
		return self.name

	def getComment(self):
		return self.comment

	def getCommand(self):
		return self.command

	def getIcon(self):
		return self.icon

	def getTerminal(self):
		return self.use_term

	def getShow(self):
		return self.show

	def getPermissions(self):
		return self.permissions
