#  Smeg - Simple Menu Editor for GNOME
#
#  Travis Watkins <alleykat@gmail.com>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
#  Boston, MA 02111-1307, USA.
#
#  (C) Copyright 2005 Travis Watkins

import os
from Smeg.IconHandler import IconHandler
import xdg.Config, xdg.IniFile, xdg.Menu, xdg.IconTheme

class PyXDGIconHandler(IconHandler):
	def __init__(self, options):
		super(PyXDGIconHandler, self).__init__(options)
		xdg.Config.cache_time = 300
		kde_theme, gnome_theme = None, None
		try:
			fd = os.popen3('gconftool-2 -g /desktop/gnome/interface/icon_theme')
			output = fd[1].readlines()
			gnome_theme = output[0].strip()
		except:
			gnome_theme = 'gnome'
		try:
			fd = os.popen3('kde-config --path config')
			output = fd[1].readlines()
			cfgdir, tmp = output[0].split(':', 1)
			config = xdg.IniFile.IniFile()
			config.parse(os.path.join(cfgdir, 'kdeglobals'), ['General'])
			theme = config.get('Theme', 'Icons')
			if theme:
				kde_theme = theme
			else:
				kde_theme = 'default.kde'
		except:
			kde_theme = 'default.kde'
		if options['desktop_environment'] == 'GNOME':
			self.theme = gnome_theme
		elif options['desktop_environment'] == 'KDE':
			self.theme = kde_theme

	def getIconPath(self, name, item_type='entry', size=48):
		super(PyXDGIconHandler, self).getIconPath(name, item_type, size)
		if '/' in name:
			if os.access(name, os.F_OK):
				return name
			else:
				name = os.path.split(name)[1]
		if name == 'main-menu':
			if self.options['desktop_environment'] == 'GNOME':
				name = 'gnome-logo-icon-transparent'
			elif self.options['desktop_environment'] == 'KDE':
				name = 'kmenu'
		path = xdg.IconTheme.getIconPath(name, size, self.theme)
		if path == None and name not in ('folder', 'gnome-fs-directory', 'application-default-icon'):
			if item_type == 'menu':
				if self.options['desktop_environment'] == 'KDE':
					path = self.getIconPath('folder', item_type, size)
				else:
					path = self.getIconPath('gnome-fs-directory', item_type, size)
			elif item_type == 'entry':
				path = self.getIconPath('application-default-icon', size)
		return path
