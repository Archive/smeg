#  Smeg - Simple Menu Editor for GNOME
#
#  Travis Watkins <alleykat@gmail.com>
#
#  This library is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library; if not, write to the
#  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
#  Boston, MA 02111-1307, USA.
#
#  (C) Copyright 2005 Travis Watkins

from Smeg.MenuHandler import MenuHandler, MenuItem
import xdg.Config, xdg.MenuEditor, xdg.BaseDirectory, xdg.Menu
import re, xml.dom.minidom, os

class PyXDGMenuHandler(MenuHandler):
	editor = None

	def __init__(self, filename, options):
		super(PyXDGMenuHandler, self).__init__(filename, options)
		xdg.Config.setWindowManager(options['desktop_environment'])
		self.editor = xdg.MenuEditor.MenuEditor(filename, root=options['root_mode'])

	def __iter__(self):
		super(PyXDGMenuHandler, self).__iter__()
		return self.__loadMenus()

	def __loadMenus(self, menu=None, depth=0):
		if not menu:
			menu = self.editor.menu
			if 'applications' in self.filename:
				icon = 'main-menu'
			else:
				icon = menu.getIcon()
			yield (MenuItem('menu', menu, menu.getName(), menu.getComment(), None, icon, None, True, 'none'), None, depth)
		depth += 1
		for item in menu.getEntries(True):
			if isinstance(item, xdg.Menu.Menu):
				if item.getName() != '':
					yield (MenuItem('menu', item, item.getName(), item.getComment(), None, item.getIcon(), None, item.Show, self.editor.getAction(item)), self.__loadMenus(item, depth), depth)
		depth -= 1

	def __copyAppDirs(self, old_parent, new_parent, menu=None):
		if menu:
			master = menu.AppDirs
		else:
			master = self.editor.menu.AppDirs
		new_menu = self.editor._MenuEditor__getXmlMenu(new_parent.getPath(True, True))
		for appdir in old_parent.AppDirs:
			if appdir not in new_parent.AppDirs:
				if menu or appdir not in master:
					self._MenuEditor__addXmlTextElement(new_menu, 'AppDir', appdir)

	def __fixAppDirs(self, item):
		for parent in item.Parents:
			menu = self.editor._MenuEditor__getXmlMenu(parent.getPath(True, True))
			appdir_path = os.path.join(xdg.BaseDirectory.xdg_data_dirs[0], 'applications')
			for node in menu.childNodes:
				if node.nodeType == xml.dom.minidom.Node.ELEMENT_NODE and node.tagName == 'AppDir':
					for child in node.childNodes:
						if child.nodeType == xml.dom.minidom.Node.TEXT_NODE:
							if child.nodeValue == appdir_path:
								return
			self.editor._MenuEditor__addXmlTextElement(menu, 'AppDir', appdir_path)

	def __copyDirectoryDirs(self, old_parent, new_parent, menu=None):
		if menu:
			master = menu.DirectoryDirs
		else:
			master = self.editor.menu.DirectoryDirs
		new_menu = self.editor._MenuEditor__getXmlMenu(new_parent.getPath(True, True))
		for dirdir in old_parent.DirectoryDirs:
			if dirdir not in new_parent.DirectoryDirs:
				if menu or dirdir not in master:
					self.editor._MenuEditor__addXmlTextElement(new_menu, 'DirectoryDir', dirdir)

	def __fixDirectoryDirs(self, item):
		menu = self.editor._MenuEditor__getXmlMenu(item.getPath(True, True))
		dirdir_path = os.path.join(xdg.BaseDirectory.xdg_data_dirs[0], 'desktop-directories')
		for node in menu.childNodes:
			if node.nodeType == xml.dom.minidom.Node.ELEMENT_NODE and node.tagName == 'DirectoryDir':
				for child in node.childNodes:
					if child.nodeType == xml.dom.minidom.Node.TEXT_NODE:
						if child.nodeValue == dirdir_path:
							return
		self.editor._MenuEditor__addXmlTextElement(menu, 'DirectoryDir', dirdir_path)

	def getEntries(self, menu):
		super(PyXDGMenuHandler, self).getEntries(menu)
		for item in menu.getEntries(True):
			if isinstance(item, xdg.Menu.Menu):
				if item.getName() != '':
					yield MenuItem('menu', item, item.getName(), item.getComment(), None, item.getIcon(), None, item.Show, self.editor.getAction(item))
			elif isinstance(item, xdg.Menu.MenuEntry):
				if '-usercustom' not in item.DesktopFileID:
					if item.Show == 'NoDisplay' or item.Show == True:
						desk = item.DesktopEntry
						yield MenuItem('entry', item, desk.getName(), desk.getComment(), desk.getExec(), desk.getIcon(), desk.getTerminal(), item.Show, self.editor.getAction(item))
			elif isinstance(item, xdg.Menu.Separator):
				yield MenuItem('seperator', item, '-----', None, None, '', None, True, 'none')

	def finish(self):
		super(PyXDGMenuHandler, self).finish()
		nodes = self.editor.doc.getElementsByTagName('Merge')
		for node in nodes:
			parent = node.parentNode
			parent.removeChild(node)
			parent.appendChild(node)
		self.editor.save()
		return True

	def move(self, item, old_parent, new_parent, before, after):
		super(PyXDGMenuHandler, self).move(item, old_parent, new_parent, before, after)
		if isinstance(item, xdg.Menu.MenuEntry):
			if new_parent.Name == 'Other':
				return False
#		if before:
#			if old_parent.Entries.index(item) == 0:
#				return False
#		if after:
#			if old_parent.Entries.index(item) == len(old_parent.Entries) - 1:
#				print old_parent.Entries, old_parent.Entries.index(item), len(old_parent.Entries)
#				return False
		if not isinstance(item, xdg.Menu.Separator):
			if old_parent != new_parent:
				self.__copyAppDirs(old_parent, new_parent, item)
			if isinstance(item, xdg.Menu.MenuEntry):
				self.editor.moveMenuEntry(item, old_parent, new_parent, after, before)
			elif isinstance(item, xdg.Menu.Menu):
				self.editor.moveMenu(item, old_parent, new_parent, after, before)
				if old_parent != new_parent:
					self.__copyDirectoryDirs(old_parent, new_parent, item)
			else:
				return False
		else:
			if old_parent == new_parent:
				return False
			self.editor.moveSeparator(item, old_parent, after, before)
		return True

	def revert(self, item):
		super(PyXDGMenuHandler, self).revert(item)
		if isinstance(item, xdg.Menu.MenuEntry):
			self.editor.revertMenuEntry(item)
		elif isinstance(item, xdg.Menu.Menu):
			self.editor.revertMenu(item)
		else:
			return False
		return True

	def new(self, item, parent):
		super(PyXDGMenuHandler, self).new(item, parent)
		if isinstance(item, (str, unicode)):
			fd = open('/tmp/smegtemp.desktop', 'w')
			fd.write(item)
			fd.close()
			deskentry = xdg.DesktopEntry.DesktopEntry('/tmp/smegtemp.desktop')
			item = MenuItem('entry', None, deskentry.getName(), deskentry.getComment(), deskentry.getExec(), deskentry.getIcon(), deskentry.getTerminal(), True, 'delete')
			item.id = self.editor.createMenuEntry(parent, item.getName(), item.getCommand(), deskentry.getGenericName(), item.getComment(), item.getIcon(), item.getTerminal())
			del deskentry
			os.unlink('/tmp/smegtemp.desktop')
		else:
			if item.getType() == 'menu':
				item.id = self.editor.createMenu(parent, item.getName(), None, item.getComment(), item.getIcon())
			elif item.getType() == 'entry':
				item.id = self.editor.createMenuEntry(parent, item.getName(), item.getCommand(), None, item.getComment(), item.getIcon(), item.getTerminal())
			elif item.getType() == 'separator':
				item.id = self.editor.createSeparator(parent)
			else:
				return False
		return item

	def delete(self, item):
		super(PyXDGMenuHandler, self).delete(item)
		if isinstance(item, xdg.Menu.MenuEntry):
			self.editor.deleteMenuEntry(item)
		elif isinstance(item, xdg.Menu.Menu):
			self.editor.deleteMenu(item)
		elif isinstance(item, xdg.Menu.Separator):
			self.editor.deleteSeparator(item)
		else:
			return False
		return True

	def toggleVisibility(self, item):
		super(PyXDGMenuHandler, self).toggleVisibility(item)
		if isinstance(item, xdg.Menu.MenuEntry):
			if item.Show == True:
				self.editor.hideMenuEntry(item)
			else:
				self.editor.unhideMenuEntry(item)
			self.__fixAppDirs(item)
		elif isinstance(item, xdg.Menu.Menu):
			if item.Show == True:
				self.editor.hideMenu(item)
			elif item.Show == 'NoDisplay' or item.Show == 'Deleted':
				self.editor.unhideMenu(item)
			elif item.Show == 'Empty':
				return False
			self.__fixDirectoryDirs(item)
		else:
			return False
		return True

	def isVisible(self, item):
		super(PyXDGMenuHandler, self).isVisible(item)
		if not isinstance(item, xdg.Menu.Separator) and item.Show == True:
			return True
		return False

	def save(self, item):
		super(PyXDGMenuHandler, self).save(item)
		if item.getType() == 'entry':
			item.id = self.editor.editMenuEntry(item.getId(), item.getName(), None, item.getComment(), item.getCommand(), item.getIcon(), item.getTerminal())
			self.__fixAppDirs(item.getId())
		elif item.getType() == 'menu':
			item.id = self.editor.editMenu(item.getId(), item.getName(), None, item.getComment(), item.getIcon())
			self.__fixDirectoryDirs(item.getId())
		else:
			return False
		return item
